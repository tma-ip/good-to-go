﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GoodToGo.Models
{
    public class Car
    {
        [Key]
        public int Id { get; set; }
        public string UserId { get; set; }
        [Required(ErrorMessage = "The registration number field is required")]
        public string Registration_number { get; set; }
        [Required(ErrorMessage = "The brand field is required")]
        public string Brand { get; set; }
        public string Details { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}