﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodToGo.Models
{
    public class Expense
    {
        [Key]
        public int Id { get; set; }
        public int CarId { get; set; }
        [Required(ErrorMessage = "The price field is required")]
        [Range(0.0, Double.MaxValue, ErrorMessage = "The price can't be a negative number")]
        public double Price { get; set; }
        [Required(ErrorMessage = "The service name field is required")]
        public string ServiceName { get; set; }
        [MaxLength(40, ErrorMessage = "Description cannot be longer than 40 characters.")]
        public string Description { get; set; }

        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        public virtual Car Car { get; set; }

        
        public IEnumerable<SelectListItem> Cars { get; set; }
    }
}