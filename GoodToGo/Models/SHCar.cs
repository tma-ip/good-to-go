﻿using Microsoft.ML.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodToGo.Models
{
    public class SHCar
    {
        [LoadColumn(1)]
        public float Price;

        [LoadColumn(2)]
        public float YearOfRegistration;

        [LoadColumn(3)]
        public string Gearbox;

        [LoadColumn(4)]
        public string Model;

        [LoadColumn(5)]
        public float Kilometer;

        [LoadColumn(6)]
        public string FuelType;

        [LoadColumn(7)]
        public string Brand;
    }

    public class SHCarPricePrediction
    {
        [ColumnName("Score")]
        public float Price;
    }
}