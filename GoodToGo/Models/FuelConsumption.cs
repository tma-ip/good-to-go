﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodToGo.Models
{
    public class FuelConsumption
    {
        [Key]
        public int Id { get; set; }
        public int CarId { get; set; }
        [Required(ErrorMessage = "The distance field is required")]
        [Range(0.0, Double.MaxValue, ErrorMessage = "The distance can't be a negative number")]
        public double Distance { get; set; }
        [Required(ErrorMessage = "The fuel quantity field is required")]
        [Range(0.0, Double.MaxValue, ErrorMessage = "The fuel quantity can't be a negative number")]
        public double Fuel { get; set; }

        public virtual Car Car { get; set; }

        // Se aduaga acest atribute pentru a putea prelua toate proiectele si statusurile
        public IEnumerable<SelectListItem> Cars { get; set; }
    }
}