﻿using GoodToGo.Models;
using GoodToGo.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodToGo.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UserController : Controller
    {
        private IUnitOfWork unitOfWork;

        public UserController()
        {
            this.unitOfWork = new UnitOfWork();
        }

        public ActionResult Index()
        {
            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }

            var users = unitOfWork.Users.GetAllUsers();
            foreach (var user in users)
            {
                user.CarList = GetCars(user.Id);
            }
            ViewBag.UsersList = users;
            return View();
        }

        public ActionResult Show(string id)
        {
            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }
            var user = unitOfWork.Users.GetUser(id);
            user.CarList = GetCars(id);
            List<int> expenses = new List<int>();
            foreach (var car in user.CarList)
            {
                expenses.Add(GetRecordsNumber(car.Id));
            }
            user.ExpensesRecorded = expenses;
            return View(user);
        }


        [NonAction]
        private IEnumerable<Car> GetCars(string userId)
        {
            return unitOfWork.Cars.SelectAllByUser(userId);
        }

        [NonAction]
        private int GetRecordsNumber(int carId)
        {
            return unitOfWork.Expenses.GetAllByCarId(carId).Count();
        }
    }
}