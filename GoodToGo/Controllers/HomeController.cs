﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodToGo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }
            ViewBag.isAdmin = User.IsInRole("Administrator");
            ViewBag.isUser = User.IsInRole("User");
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "The app in which you can track your car expenses and estimate the" +
                " quantity of fuel needed to get from a location to another.";

            return View();
        }
    }
}