﻿using GoodToGo.Models;
using GoodToGo.Repository;
using Microsoft.AspNet.Identity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace GoodToGo.Controllers
{
    [Authorize]
    public class ExpenseController : Controller
    {
        private IUnitOfWork unitOfWork;
        private List<String> puns;

        public ExpenseController()
        {
            this.unitOfWork = new UnitOfWork();
            this.puns = new List<String> { "There's a lot of stress... but once you get in the car, all that goes out the window.",
            "A dream without ambition is like a car without gas... you're not going anywhere.",
            "What do you call a Ford Fiesta that ran out of gas?... A Ford Siesta.",
            "Car puns are exhausting.",
            " I ran my Subi into a lake... Now it’s a Scubaru.",
            " What part of the car is the laziest?... The wheels, because they are always tired.",
            "What’s a car’s favorite meal?... Brake-fast."};
        }

        public ActionResult New()
        {
            ViewBag.Image = "../../Content/Img/masina3_scalata.jpg";
            Expense expense = new Expense();
            expense.Cars = GetAllCars();
            expense.Date = DateTime.Now;
            ViewBag.MaxDate = DateTime.Now.ToString("yyyy-MM-dd");
            ViewBag.Puns = puns;
            return View(expense);
        }

        [HttpPost]
        public ActionResult New(Expense expense)
        {
            ViewBag.Puns = puns;
            try
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.Expenses.Insert(expense);
                    unitOfWork.Save();
                    TempData["message"] = "A new expense record has been added!";
                    return RedirectToAction("Index", "Expense");
                }
                else
                {
                    
                    expense.Cars = GetAllCars();
                    ViewBag.MaxDate = DateTime.Now.ToString("yyyy-MM-dd");
                    return View(expense);
                }
            }
            catch (Exception e)
            {
                expense.Cars = GetAllCars();
                ViewBag.MaxDate = DateTime.Now.ToString("yyyy-MM-dd");
                return View(expense);
            }
        }

        [HttpGet]
        public ActionResult Index(int? carId)
        {
            ViewBag.Image = "../../Content/Img/masina3_scalata.jpg";
            if (carId != null)
            {
                
                List<Expense> expense;
                expense = unitOfWork.Expenses.GetAllByCarId(carId??0).ToList();
                ViewBag.Expense = expense;
                ViewBag.carName = unitOfWork.Cars.GetName(carId??0);
                if (TempData.ContainsKey("message"))
                {
                    ViewBag.message = TempData["message"].ToString();
                }
                return View();
            }
            else
            {
                List<Car> cars;
                var userId = User.Identity.GetUserId();
                cars = unitOfWork.Cars.SelectAllByUser(userId).ToList();
                ViewBag.Cars = cars;
                List<List<Expense>> expenses = new List<List<Expense>>();
                for (int i = 0; i < cars.Count(); i++)
                {
                    expenses.Add(new List<Expense>());
                    int id = cars[i].Id;
                    expenses[i] = unitOfWork.Expenses.GetAllByCarId(id).ToList();       
                }
                ViewBag.ExpenseList = expenses;
                List<string> carNames = new List<string>();
                foreach (var car in cars)
                {
                    carNames.Add(car.Registration_number);
                }
                ViewBag.carNameList = carNames;
                ViewBag.nbCars = cars.Count();
                if (TempData.ContainsKey("message"))
                {
                    ViewBag.message = TempData["message"].ToString();
                }
                return View();
            }
        }

        public ActionResult Show(int id)
        {
            ViewBag.Image = "../../Content/Img/masina3_scalata.jpg";
            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }
            Expense expense = unitOfWork.Expenses.GetByID(id);
            ViewBag.Project = expense;
            return View(expense);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.Puns = puns;
            ViewBag.Image = "../../Content/Img/masina3_scalata.jpg";
            Expense expense = unitOfWork.Expenses.GetByID(id);
            expense.Cars = GetAllCars();
            ViewBag.Expense = expense;
            ViewBag.MaxDate = DateTime.Now.ToString("yyyy-MM-dd");
            return View(expense);
        }

        [HttpPut]
        public ActionResult Edit(int id, Expense requestExpense)
        {
            requestExpense.Cars = GetAllCars();
            ViewBag.Puns = puns;
            try
            {
                if (ModelState.IsValid)
                {
                    Expense expense = unitOfWork.Expenses.GetByID(id);
                    if (TryUpdateModel(expense))
                    {
                        expense.Price = requestExpense.Price;
                        expense.Description= requestExpense.Description;
                        expense.ServiceName = requestExpense.ServiceName;
                        expense.Date = requestExpense.Date;
                        unitOfWork.Save();
                        TempData["message"] = "The expense record has been edited!";
                    }
                    return RedirectToAction("Show", new { id = expense.Id });
                }
                else
                {
                    ViewBag.MaxDate = DateTime.Now.ToString("yyyy-MM-dd");
                    return View(requestExpense);
                }
            }
            catch (Exception e)
            {
                return Redirect("~");
            }
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            unitOfWork.Expenses.Delete(id);
            unitOfWork.Save();
            TempData["message"] = "The record was deleted";
            return RedirectToAction("Index", "Expense");
        }

        [NonAction]
        public IEnumerable<SelectListItem> GetAllCars()
        {
            var selectList = new List<SelectListItem>();
            string userId = User.Identity.GetUserId();
            var cars = unitOfWork.Cars.SelectAllByUser(userId);

            foreach (var car in cars)
            {
                selectList.Add(new SelectListItem
                {
                    Value = car.Id.ToString(),
                    Text = car.Registration_number
                });
            }
            return selectList;
        }

    }
}