﻿using GoodToGo.Models;
using GoodToGo.Repository;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodToGo.Controllers
{
    [Authorize]
    public class CarController : Controller
    {
        private IUnitOfWork unitOfWork;
        private List<String> puns;


        public CarController()
        {
            this.unitOfWork = new UnitOfWork();
            this.puns = new List<String> { "There's a lot of stress... but once you get in the car, all that goes out the window.",
            "A dream without ambition is like a car without gas... you're not going anywhere.",
            "What do you call a Ford Fiesta that ran out of gas?... A Ford Siesta.",
            "Car puns are exhausting.",
            " I ran my Subi into a lake... Now it’s a Scubaru.",
            " What part of the car is the laziest?... The wheels, because they are always tired.",
            "What’s a car’s favorite meal?... Brake-fast."};
        }

        public CarController(IUnitOfWork uow)
        {
            this.unitOfWork = uow;
        }

        public ActionResult New()
        {
            Car car = new Car();
            ViewBag.Image = "../../Content/Img/masina2_scalata.jpg";
            var userId = User.Identity.GetUserId();
            ViewBag.Cars = unitOfWork.Cars.SelectAllByUser(userId);
            ViewBag.Puns = puns;
            return View(car);
        }

        [HttpPost]
        public ActionResult New(Car car)
        {
            var userId = User.Identity.GetUserId();
            ViewBag.Cars = unitOfWork.Cars.SelectAllByUser(userId);
            ViewBag.Puns = puns;
            try
            {
                if (ModelState.IsValid)
                {
                    car.UserId = User.Identity.GetUserId();
                    unitOfWork.Cars.Insert(car);
                    unitOfWork.Save();
                    TempData["message"] = "A new car has been added!";
                    return RedirectToAction("Index", "Car");
                }
                else
                {
                    return View(car);
                }
            }
            catch (Exception e)
            {
                return View(car);
            }
        }

        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Image = "../../Content/Img/masina2_scalata.jpg";
            var userId = User.Identity.GetUserId();
            ViewBag.Cars = unitOfWork.Cars.SelectAllByUser(userId);

            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }
            return View("Index");
        }

        public ActionResult Show(int id)
        {
            ViewBag.Image = "../../Content/Img/masina2_scalata.jpg";
            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }
            Car car = unitOfWork.Cars.SelectByID(id);
            return View(car);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.Image = "../../Content/Img/masina2_scalata.jpg";
            Car car = unitOfWork.Cars.SelectByID(id);
            ViewBag.Car = car;
            return View(car);
        }

        [HttpPut]
        public ActionResult Edit(int id, Car requestCar)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Car car = unitOfWork.Cars.SelectByID(id);
                    if (TryUpdateModel(car))
                    {
                        car.Registration_number = requestCar.Registration_number;
                        car.Details = requestCar.Details;
                        car.Brand = requestCar.Brand;
                        unitOfWork.Save();
                        TempData["message"] = "The car has been edited!";
                    }
                    return RedirectToAction("Show", new { id = car.Id });
                }
                else
                {
                    return View(requestCar);
                }
            }
            catch (Exception e)
            {
                return Redirect("~");
            }
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            unitOfWork.Cars.Delete(id);
            unitOfWork.Save();
            TempData["message"] = "The car was deleted";
            return RedirectToAction("Index", "Car");

        }
    }
}