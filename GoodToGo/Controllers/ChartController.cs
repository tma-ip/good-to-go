﻿using GoodToGo.Models;
using GoodToGo.Repository;
using Microsoft.AspNet.Identity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace GoodToGo.Controllers
{
    [Authorize]
    public class ChartController : Controller
    {
        private IUnitOfWork unitOfWork;

        public ChartController()
        {
            this.unitOfWork = new UnitOfWork();
        }

        public ActionResult TrackExpenses( int? requestMonth, int? requestYear, int? requestCarId)
        {
            ViewBag.Image = "../../Content/Img/masina3_scalata.jpg";
            var userId = User.Identity.GetUserId();
            int[] daysNumber = new int[32];
            var selectList = new List<SelectListItem>();
            var cars = unitOfWork.Cars.SelectAllByUser(userId);
            foreach (var car in cars)
            {
                selectList.Add(new SelectListItem
                {
                    Value = car.Id.ToString(),
                    Text = car.Registration_number
                });
            }
            ViewBag.Cars = selectList;

            int minYear = 2019, maxYear = 0, diffBetweenMaxMinYear;

            foreach (var e in unitOfWork.Expenses.GetAll())
            {

                if (e.Date.Year > maxYear)
                    maxYear = e.Date.Year;
                if (e.Date.Year < minYear)
                    minYear = e.Date.Year;
            }

            diffBetweenMaxMinYear = Math.Abs( maxYear - minYear);
          
            var years = Enumerable.Range(minYear, diffBetweenMaxMinYear + 1).Select(i => new SelectListItem { Value = i.ToString(), Text = i.ToString() });

            ViewBag.Years = years;
            ViewBag.CarId = requestCarId;
            ViewBag.Month = requestMonth;
            ViewBag.Year = requestYear;

            return View();
            
        }
        
        
        public ActionResult AnnualChart(int? requestYear, int? requestCarId)
        {
            ViewBag.Image = "../../Content/Img/masina3_scalata.jpg";
            var userId = User.Identity.GetUserId();
            var selectList = new List<SelectListItem>();
            var cars = unitOfWork.Cars.SelectAllByUser(userId);
            foreach (var car in cars)
            {
                selectList.Add(new SelectListItem
                {
                    Value = car.Id.ToString(),
                    Text = car. Registration_number
                });
            }
            ViewBag.Cars = selectList; 

            int minYear = 2019, maxYear = 0, diffBetweenMaxMinYear;

            foreach (var e in unitOfWork.Expenses.GetAll())
            {

                if (e.Date.Year > maxYear)
                    maxYear = e.Date.Year;
                if (e.Date.Year < minYear)
                    minYear = e.Date.Year;
            }

            diffBetweenMaxMinYear = Math.Abs(maxYear - minYear);

            var years = Enumerable.Range(minYear, diffBetweenMaxMinYear + 1).Select(i => new SelectListItem { Value = i.ToString(), Text = i.ToString() });

            ViewBag.Years = years;
            ViewBag.CarId = requestCarId;
            ViewBag.Year = requestYear;

            return View();


        }

        public ActionResult ChartPerMonth(int requestMonth, int requestYear, int requestCarId)
        {
            List<Expense> expense;
            expense = unitOfWork.Expenses.GetAllByCarId(requestCarId).ToList();

            ArrayList xValue = new ArrayList();
            ArrayList yValue = new ArrayList();

            double[] days = new double[31];

            foreach (var e in expense)
            {
                
                if (e.Date.Month == requestMonth && e.Date.Year == requestYear)
                {
                    days[e.Date.Day-1] += e.Price; //ADUNG LA ZIUA RESPECTIVA TOATE CHELTUIELILE
                }

            }

            for (int i = 0; i < GetNumberDays(requestMonth,requestYear); i++)
            {
                xValue.Add(i+1);
                yValue.Add(days[i]);

            }

            DateTime dtDate = new DateTime(2019, requestMonth, 1);
            string sMonthFullName = dtDate.ToString("MMMM");
            string temp = @"<Chart>
                      <ChartAreas>
                        <ChartArea Name=""Default"" _Template_=""All"">
                          <AxisY>
                            <LabelStyle Font=""Verdana, 12px"" />
                          </AxisY>
                          <AxisX LineColor=""64, 64, 64, 64"" Interval=""1"">
                            <LabelStyle Font=""Verdana, 12px"" />
                          </AxisX>
                        </ChartArea>
                      </ChartAreas>
                    </Chart>";

            var chart = new Chart(width:700, height: 500, theme: temp)
                .SetXAxis ("Days", 1, GetNumberDays(requestMonth,requestYear))
                .SetYAxis("Money spent")
                .AddTitle("Expenses per month " + sMonthFullName)
                .AddLegend()
                .AddSeries(
                    chartType: "Column",
                    name: "Dollars",
                    xValue: xValue,
                    yValues: yValue)
                .GetBytes("png");

            return File(chart, "image/png");
        }

        public ActionResult ChartPerYear(int requestYear, int requestCarId)
        {
            List<Expense> expense;
            expense = unitOfWork.Expenses.GetAllByCarId(requestCarId).ToList();

            ArrayList xValue = new ArrayList();
            ArrayList yValue = new ArrayList();

            double[] monthsExpenses = new double[12];

            foreach (var e in expense)
            {
                //caut cheltuilile din anul respectiv
                if (e.Date.Year == requestYear)
                {
                    monthsExpenses[e.Date.Month-1] += e.Price; //Adun toate cheltuielile din luna
                }

            }

            for (int i = 0; i < monthsExpenses.Length  ; i++)
            {
               // Debug.Write(days[i]);
                xValue.Add(i+1);
                yValue.Add(monthsExpenses[i]);

            }
            //new[] {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"}
            string temp = @"<Chart>
                      <ChartAreas>
                        <ChartArea Name=""Default"" _Template_=""All"">
                          <AxisY>
                            <LabelStyle Font=""Verdana, 12px"" />
                          </AxisY>
                          <AxisX LineColor=""64, 64, 64, 64"" Interval=""1"">
                            <LabelStyle Font=""Verdana, 12px"" />
                          </AxisX>
                        </ChartArea>
                      </ChartAreas>
                    </Chart>";


            var chart = new Chart(width: 700, height: 500, theme: temp)
                .SetXAxis("Months", 1, 12)
                .SetYAxis("Money spent")
                .AddLegend()
                .AddSeries(
                    chartType: "Column",
                    name:"Dollars",
                    xValue: new[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" },
                    yValues: yValue)
                .GetBytes("png");

            return File(chart, "image/png");
        }

        //Functie care imi returneaza numarul de zile ale unei luni
        public int GetNumberDays(int Month, int Year)
        {
            if (Month == 1 || Month == 3 || Month == 5 || Month == 7 || Month == 8 || Month == 10 || Month == 12)
                return 31;
            else
                if (Month == 4 || Month == 6 || Month == 9 || Month == 11)
                return 30;
            else
                 if (Month == 2 && Year % 400 == 0)
                return 29;
            else
                if (Month == 2 && Year % 4 == 0 && Year % 100 != 0)
                return 29;
            else
                return 28;

        }

    }
}