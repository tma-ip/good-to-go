﻿using GoodToGo.Models;
using Microsoft.ML;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodToGo.Controllers
{
    [Authorize]
    public class SHCarController : Controller
    {

        public ActionResult EstimatePrice(float? YearOfRegistration, string Gearbox, string Model, float? Kilometer, string FuelType, string Brand)
        {
            CreateLists();
            //ReadCarDataFromCSV();
            ReadCarBrandsTXT();
            ViewBag.Year = DateTime.Today.ToString("yyyy");
            if (YearOfRegistration == null || string.IsNullOrEmpty(Gearbox) || string.IsNullOrEmpty(Model) || Kilometer == null || string.IsNullOrEmpty(FuelType) || string.IsNullOrEmpty(Brand))
            {
                return View();
            }

            MLContext mlContext = new MLContext(seed: 0);
            string modelPath = "~\\Content\\Data\\Model.zip";
            ITransformer loadedModel;
            using (var stream = new FileStream(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                loadedModel = mlContext.Model.Load(stream);
            }

            var predictionFunction = loadedModel.CreatePredictionEngine<SHCar, SHCarPricePrediction>(mlContext);
            var carSample0 = new SHCar()
            {
                Price = 0,
                YearOfRegistration = 2011,
                Gearbox = "manuell",
                Model = "tiguan",
                Kilometer = 30000,
                FuelType = "benzin",
                Brand = "volkswagen"

            };

            var carSample = new SHCar()
            {
                Price = 0,
                YearOfRegistration = YearOfRegistration ?? 0,
                Gearbox = Gearbox,
                Model = Model,
                Kilometer = Kilometer ?? 0,
                FuelType = FuelType,
                Brand = Brand
            };

            var prediction = predictionFunction.Predict(carSample);
            ViewBag.Prediction = prediction.Price;
            return View();
        }

        private void CreateLists()
        {
            #region Gearbox
            List<SelectListItem>  selectListGearbox = new List<SelectListItem>();

            selectListGearbox.Add(new SelectListItem
            {
                    Value = "manuell",
                    Text = "manual"
            });

            selectListGearbox.Add(new SelectListItem
            {
                Value = "automatik",
                Text = "automated"
            });
            ViewBag.Gearbox = selectListGearbox;
            #endregion
            #region Fuel
            List<SelectListItem> selectListFuel= new List<SelectListItem>();

            selectListFuel.Add(new SelectListItem
            {
                Value = "benzin",
                Text = "benzine"
            });

            selectListFuel.Add(new SelectListItem
            {
                Value = "diesel",
                Text = "diesel"
            });
            ViewBag.Fuel = selectListFuel;
            #endregion
        }

        private void ReadCarDataFromCSV()
        {
            string modelPath = "~\\Content\\Data\\models.csv";
            string brandPath = "~\\Content\\Data\\brands.csv";

            List<SelectListItem> selectListModels = new List<SelectListItem>();
            List<SelectListItem> selectListBrands = new List<SelectListItem>();

            bool repeat = true;
            using (var stream = new StreamReader(Server.MapPath(modelPath)))
            {
                string line;
                while (!stream.EndOfStream)
                {
                    line = stream.ReadLine();
                    if (repeat)
                    {
                        line = stream.ReadLine();
                        repeat = false;
                    }

                   string v = GetWantedString(line);
                   selectListModels.Add(new SelectListItem
                   {
                       Value = v,
                       Text = FormatAndTranslate(v)
                   });
                }
            }
            repeat = true;
            using (var stream = new StreamReader(Server.MapPath(brandPath)))
            {
                string line;
                while (!stream.EndOfStream)
                {
                    line = stream.ReadLine();
                    if (repeat)
                    {
                        line = stream.ReadLine();
                        repeat = false; 
                    }

                    string v = GetWantedString(line);
                    selectListBrands.Add(new SelectListItem
                    {
                            Value = v,
                            Text = v[0].ToString().ToUpper() + v.Substring(1)
                    });                 
                }
            }

            ViewBag.models = selectListModels;
            ViewBag.brands = selectListBrands;
        }

        public JsonResult GetModels(string brand)
        {

            string path = "~\\Content\\Data\\ModelsAndBrands.txt";
            List<SelectListItem> temp = new List<SelectListItem>();
            if (brand == "Other")
            {
                temp.Add(new SelectListItem
                {
                    Value = "Other",
                    Text = "Other"
                });
            }
            else
            {
                using (var stream = new StreamReader(Server.MapPath(path)))
                {
                    string line;
                    while (!stream.EndOfStream)
                    {
                        line = stream.ReadLine();
                        if (line.Equals("------"))
                        {
                            line = stream.ReadLine();
                            if (line.Equals(brand))
                            {
                                line = stream.ReadLine();
                                while (!stream.EndOfStream && !line.Equals("------"))
                                {
                                    temp.Add(new SelectListItem
                                    {
                                        Value = line,
                                        Text = FormatAndTranslate(line)
                                    });
                                    line = stream.ReadLine();
                                }
                                break;
                            }
                        }
                    }
                }
            }
            return Json(temp, JsonRequestBehavior.AllowGet);
        }

        private void ReadCarBrandsTXT()
        {
            string path = "~\\Content\\Data\\ModelsAndBrands.txt";

            List<SelectListItem> selectListBrands = new List<SelectListItem>();

            using (var stream = new StreamReader(Server.MapPath(path)))
            {
                string line;
                while (!stream.EndOfStream)
                {
                    line = stream.ReadLine();
                    if (line == "------")
                    {
                        
                        
                        line = stream.ReadLine();
                        selectListBrands.Add(new SelectListItem
                        {
                            Value = line,
                            Text = FormatAndTranslate(line)
                        });
                    }
                }
            }
            selectListBrands.Add(new SelectListItem
            {
                Value = "Other",
                Text = "Other"
            });
            ViewBag.brands = selectListBrands;
        }


        private string GetWantedString(string s)
        {
            var charsToRemove = new string[] {"\"", ",", "."};
            
            int start = 0;
            int i;
            for (i = 1; i < s.Count(); i++)
            {
                if (s[i] == '\"' && s[i-1] >= '0' && s[i-1] <= '9') 
                break;
            }
            int stop = i;
            string s1 = s.Remove(start, stop);
            foreach (var c in charsToRemove)
            {
                s1 = s1.Replace(c, string.Empty);
            }
            return s1;
        }

        private void LowerStrings(ref string model, ref string brand)
        {
            model = model.ToLower();
            brand = brand.ToLower();
        }

        private string FormatAndTranslate(string s)
        {
            s = s.Replace("klasse", "class");
            s = s.Replace("andere", "other");
            s = s.Replace("_", " ");
            s = s.Replace("reihe", "line");
            s = s.Replace("serie", "series");
            if (s[0] >= 'a' && s[0] <= 'z')
            {
                s = s[0].ToString().ToUpper() + s.Substring(1); 
            }
            return s;
        }
    }

}