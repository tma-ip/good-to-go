﻿using GoodToGo.Models;
using GoodToGo.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GoodToGo.Controllers
{
    [Authorize]
    public class EstimationController : Controller
    {
        private IUnitOfWork unitOfWork;

        public EstimationController()
        {
            this.unitOfWork = new UnitOfWork();
        }

        public ActionResult CalculateDistance(string from, string to, int? selectedCarId)
        {
            ViewBag.Image = "../../Content/Img/masina4_scalata.jpg";
            ViewBag.Distance = "";
            ViewBag.Error = "";
            var user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
            user.Cars = GetCars(user.Id);
            if (string.IsNullOrEmpty(from) || string.IsNullOrEmpty(to) || !selectedCarId.HasValue)
            {
                return View(user);
            }
            else
            {
                string url = "https://maps.googleapis.com/maps/api/directions/json?origin={0}&destination={1}&key=AIzaSyCBdx8g3xrj_-mDn8id3MDMP5hcjXeStcQ";
                url = string.Format(url, from, to);
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                StreamReader reader = new StreamReader(data);
                string responseFromServer = reader.ReadToEnd();
                response.Close();
                JToken token = JObject.Parse(responseFromServer);
                string status = token.SelectToken("status").ToString();
                if (status == "OK")
                {
                    decimal distance = (long)token.SelectToken("routes[0]").SelectToken("legs[0]")
                                        .SelectToken("distance").SelectToken("value");
                    distance = Decimal.Divide(distance, 1000);
                    float d = (float)distance;
                    ViewBag.Distance = d.ToString();
                    ViewBag.From = from.First().ToString().ToUpper() + from.Substring(1);
                    ViewBag.To = to.First().ToString().ToUpper() + to.Substring(1);
                    int carId = selectedCarId ?? (-1);
                    if (ShowMessage(carId) == false)
                    {
                        //SingletonTrainPredict.Instance.LROrdinaryLeastSquares(GetData(selectedCarId.Value));
                        //var estimationS = SingletonTrainPredict.Instance.PredictOrdinaryLeastSquares((float)distance);
                        SingletonTrainPredict.Instance.LROrdinaryLeastSquares(GetData(selectedCarId.Value));
                        var estimation = SingletonTrainPredict.Instance.PredictOrdinaryLeastSquares((float)distance);
                        ViewBag.Estimation = estimation.ToString();
                    }
                    else
                    {
                        string carName = GetCarName(carId);
                        ViewBag.EstimationError = "You need at least 5 fuel records for your chosen car, " + carName +",  to make an estimation. Please add more records.";
                    }
                }
                else
                {
                    ViewBag.Error = "A route can not be found.";
                }
                return View(user);
            }
        }

        private string GetCarName(int carId)
        {
            if (carId == -1)
                return "";
            return unitOfWork.Cars.GetName(carId);
        }

        private int GetRecordsNumber(int carId)
        {
           return unitOfWork.FuelConsumptions.GetAllByCarId(carId).Count();           
        }

        private bool ShowMessage(int carId)
        {
            if (carId != -1)
            {
                if (GetRecordsNumber(carId) < 5)
                    return true;
            }
            return false;
        }

        public List<FuelConsumption> GetData(int carId)
        {
            return unitOfWork.FuelConsumptions.GetAllByCarId(carId).ToList();
        }

        [NonAction]
        public IEnumerable<SelectListItem> GetCars(string userId)
        {
            var selectList = new List<SelectListItem>();
            var cars = unitOfWork.Cars.SelectAllByUser(userId);
            foreach (var car in cars)
            {
                selectList.Add(new SelectListItem
                {
                    Value = car.Id.ToString(),
                    Text = car.Registration_number
                });
            }
            return selectList;
        }
    }
}