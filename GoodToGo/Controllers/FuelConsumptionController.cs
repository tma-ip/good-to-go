﻿using GoodToGo.Models;
using GoodToGo.Repository;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodToGo.Controllers
{
    [Authorize]
    public class FuelConsumptionController : Controller
    {
        private IUnitOfWork unitOfWork;

        public FuelConsumptionController()
        {
            this.unitOfWork = new UnitOfWork();
        }

        public ActionResult New()
        {
            ViewBag.Image = "../../Content/Img/masina4_scalata.jpg";
            FuelConsumption fuelC = new FuelConsumption();
            fuelC.Cars = GetAllCars();
            return View(fuelC);
        }

        [HttpPost]
        public ActionResult New(FuelConsumption fuelC)
        {
            ViewBag.Image = "../../Content/Img/masina4_scalata.jpg";
            try
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.FuelConsumptions.Insert(fuelC);
                    unitOfWork.Save();
                    TempData["message"] = "A new fuel consumption record has been added!";
                    return RedirectToAction("Index", "FuelConsumption");
                }
                else
                {
                    fuelC.Cars = GetAllCars();
                    return View(fuelC);
                }
            }
            catch (Exception e)
            {
                fuelC.Cars = GetAllCars();
                return View(fuelC);
            }
        }

        [HttpGet]
        public ActionResult Index(int? carId)
        {
            ViewBag.Image = "../../Content/Img/masina4_scalata.jpg";
            if (carId != null)
            {
                List<FuelConsumption> fuelC;
                fuelC = unitOfWork.FuelConsumptions.GetAllByCarId(carId??0).ToList();
                ViewBag.FuelConsumption = fuelC;
                ViewBag.carName = unitOfWork.Cars.GetName(carId??0);
                if (TempData.ContainsKey("message"))
                {
                    ViewBag.message = TempData["message"].ToString();
                }
                return View();
            }
            else
            {
                List<Car> cars;
                var userId = User.Identity.GetUserId();
                cars = unitOfWork.Cars.SelectAllByUser(userId).ToList();
                ViewBag.Cars = cars;
                List<List<FuelConsumption>> fuelC = new List<List<FuelConsumption>>();
                for (int i = 0; i < cars.Count(); i++)
                {
                    fuelC.Add(new List<FuelConsumption>());
                    int id = cars[i].Id;
                    fuelC[i] = unitOfWork.FuelConsumptions.GetAllByCarId(id).ToList();
                }
                ViewBag.FuelConsumptionList = fuelC;
                List<string> carRegistrationNumbers = new List<string>();
                foreach (var car in cars)
                {
                    carRegistrationNumbers.Add(car.Registration_number);
                }
                ViewBag.carRegistrationNumberList = carRegistrationNumbers ;
                ViewBag.nbCars = cars.Count();
                if (TempData.ContainsKey("message"))
                {
                    ViewBag.message = TempData["message"].ToString();
                }
                return View();
            }
        }

        public ActionResult Show(int id)
        {
            ViewBag.Image = "../../Content/Img/masina4_scalata.jpg";
            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }
            FuelConsumption fuelC = unitOfWork.FuelConsumptions.GetByID(id);
            ViewBag.Project = fuelC;
            return View(fuelC);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.Image = "../../Content/Img/masina4_scalata.jpg";
            FuelConsumption fuelC = unitOfWork.FuelConsumptions.GetByID(id);
            fuelC.Cars = GetAllCars();
            ViewBag.FuelConsumption = fuelC;
            return View(fuelC);
        }

        [HttpPut]
        public ActionResult Edit(int id, FuelConsumption requestFuelC)
        {
            requestFuelC.Cars = GetAllCars();

            try
            {
                if (ModelState.IsValid)
                {
                    FuelConsumption fuelC = unitOfWork.FuelConsumptions.GetByID(id);
                    if (TryUpdateModel(fuelC))
                    {
                        fuelC.Distance = requestFuelC.Distance;
                        fuelC.Fuel = requestFuelC.Fuel;
                        unitOfWork.Save();
                        TempData["message"] = "The fuel consumption record has been edited!";
                    }
                    return RedirectToAction("Show", new { id = fuelC.Id });
                }
                else
                {
                    return View(requestFuelC);
                }
            }
            catch (Exception e)
            {
                return Redirect("~");
            }
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            unitOfWork.FuelConsumptions.Delete(id);
            unitOfWork.Save();
            TempData["message"] = "The record was deleted";
            return RedirectToAction("Index", "FuelConsumption");
        }

        [NonAction]
        public IEnumerable<SelectListItem> GetAllCars()
        {
            var selectList = new List<SelectListItem>();
            string userId = User.Identity.GetUserId();
            var cars = unitOfWork.Cars.SelectAllByUser(userId);

            foreach (var car in cars)
            {
                selectList.Add(new SelectListItem
                {
                    Value = car.Id.ToString(),
                    Text = car.Registration_number
                });
            }
            return selectList;
        }
    }
}