﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodToGo.Controllers
{
    [TestClass]
    public class HomeControllerTest : Controller
    {
        [TestMethod]
        public void About()
        {
            // Arrange
            HomeController controller = new HomeController();
            // Act
            ViewResult result = controller.About() as ViewResult;
            // Assert
            Assert.AreEqual("The app in which you can track your car expenses and estimate the" +
                " quantity of fuel needed to get from a location to another.", result.ViewBag.Message);
        }
    }
}