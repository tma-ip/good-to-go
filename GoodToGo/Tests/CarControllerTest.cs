﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GoodToGo.Models;
using GoodToGo.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Security.Principal;
using Microsoft.AspNet.Identity;

namespace GoodToGo.Controllers
{
    [TestClass]
    public class CarControllerTest
    {
        /*CarController CreateCarControllerAs(string userName)
        {

            var mock = new Mock<ControllerContext>();
            mock.SetupGet(p => p.HttpContext.User.Identity.Name).Returns(userName);
            mock.SetupGet(p => p.HttpContext.Request.IsAuthenticated).Returns(true);
            mock.Setup(p => p.HttpContext.User.Identity.GetUserId()).Returns("id");
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(x => x.Cars.SelectAllByUser(userName)).Returns(new List<Car>());

            var controller = new CarController(unitOfWorkMock.Object);
            controller.ControllerContext = mock.Object;

            return controller;
        }*/
        CarController CreateCarControllerAs(string userName)
        {
            var mock = new Mock<ControllerContext>();
            var principal = new Moq.Mock<IPrincipal>();
            //principal.Setup(p => p.IsInRole("Administrator")).Returns(true);
            principal.SetupGet(x => x.Identity.Name).Returns(userName);
            mock.SetupGet(x => x.HttpContext.User).Returns(principal.Object);
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(x => x.Cars.SelectAllByUser(userName)).Returns(new List<Car>());

            var controller = new CarController(unitOfWorkMock.Object);
            controller.ControllerContext = mock.Object;

            return controller;
        }

        CarController CreateCarControllerNew(string userName)
        {

            var mock = new Mock<ControllerContext>();
            var principal = new Moq.Mock<IPrincipal>();
            //principal.Setup(p => p.IsInRole("Administrator")).Returns(true);
            principal.SetupGet(x => x.Identity.Name).Returns(userName);
            mock.SetupGet(x => x.HttpContext.User).Returns(principal.Object);
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(x => x.Cars.SelectAllByUser(userName)).Returns(new List<Car>());
            unitOfWorkMock.Setup(x => x.Cars.Insert(It.IsAny<Car>())).Verifiable();
            unitOfWorkMock.Setup(x => x.Save()).Verifiable();

            var controller = new CarController(unitOfWorkMock.Object);
            controller.ControllerContext = mock.Object;

            return controller;
        }

        [TestMethod]
        public void IndexAction_Should_Return_IndexView_When_ValidOwner()
        {

            // Arrange
            var controller = CreateCarControllerAs("SomeUser");

            // Act
            var result = controller.Index() as ViewResult;

            // Assert
            Assert.AreEqual("Index", result.ViewName);
        }

        [TestMethod]
        public void NewActionParam_Should_Return_IndexView_When_ValidCar()
        {

            // Arrange
            var controller = CreateCarControllerNew("SomeUser");

            //valid pentru testul asta
            Car car = new Car
            {
                //Id = 1,
                UserId = "jbfxghjk",
                Registration_number = "sdsdg",
                Brand = "",
                Details = ""
            };

            //Act
            RedirectToRouteResult redirectResult = (RedirectToRouteResult)controller.New(car);

            //Assert
            Assert.AreEqual(redirectResult.RouteValues.Values.ElementAt(1) , "Car");
            Assert.AreEqual(redirectResult.RouteValues.Values.ElementAt(0), "Index");
        }

        [TestMethod]
        public void NewActionParam_Should_Return_NewView_When_NotValidCar()
        {

            // Arrange
            var controller = CreateCarControllerNew("SomeUser");

            //valid pentru testul asta
            Car car = new Car();

            // Act
            var result = controller.New(null) as ViewResult;

            // Assert
            Assert.AreEqual("", result.ViewName);
        }
    }
}