﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using GoodToGo.Models;

namespace GoodToGo.Repository
{
    public class CarRepository : GenericRepository<Car>, ICarRepository
    {
        public CarRepository(ApplicationDbContext context) : base(context)
        { }

        public IEnumerable<Car> SelectAllByUser(string userId)
        {
            return context.Cars.Include("User").Where(x => x.UserId == userId).ToList();
        }

        public Car SelectByID(int id)
        {
            return context.Cars.Where(x => x.Id == id).Include(i => i.User).FirstOrDefault();
        }

        public string GetName(int carId)
        {
            return context.Cars.Where(x => carId == x.Id).Select(x => x.Registration_number).FirstOrDefault();
        }
    }
}