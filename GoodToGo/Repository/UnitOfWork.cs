﻿using GoodToGo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodToGo.Repository
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private ApplicationDbContext context = new ApplicationDbContext();
        public IUserRepository Users { get; private set; }
        public ICarRepository Cars { get; private set; }
        public IExpenseRepository Expenses { get; private set; }
        public IFuelConsumptionRepository FuelConsumptions { get; private set; }

        public UnitOfWork()
        {
            Users = new UserRepository(context) as IUserRepository;
            Cars = new CarRepository(context) as ICarRepository;
            Expenses = new ExpenseRepository(context) as IExpenseRepository;
            FuelConsumptions = new FuelConsumptionRepository(context) as IFuelConsumptionRepository;
        }

        public int Save()
        {
              return  context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
                Dispose(true);
                GC.SuppressFinalize(this);
        }
    }
}
