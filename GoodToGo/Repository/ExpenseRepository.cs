﻿using GoodToGo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodToGo.Repository
{
    public class ExpenseRepository : GenericRepository<Expense>, IExpenseRepository
    {
        public ExpenseRepository(ApplicationDbContext context) : base(context)
        { }

        //implementare rest de metode
        public IEnumerable<Expense> GetAllByCarId(int carId)
        {
            return context.Expenses.Where(x => x.CarId == carId).ToList();
        }

        public IEnumerable<Expense> GetAll()
        {
            return context.Expenses.ToList();
        }

    }
}