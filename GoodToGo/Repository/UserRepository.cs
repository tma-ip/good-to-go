﻿using GoodToGo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodToGo.Repository
{
    public class UserRepository : GenericRepository<ApplicationUser>, IUserRepository
    {
        public UserRepository(ApplicationDbContext context) : base(context)
        { }

        public IEnumerable<ApplicationUser> GetAllUsers()
        {
            var users = from user in context.Users orderby user.UserName select user;
            return users;
        }

        public ApplicationUser GetUser(string id)
        {
            var user = context.Users.Where(x => x.Id == id).FirstOrDefault();
            return user;
        }
    }
}