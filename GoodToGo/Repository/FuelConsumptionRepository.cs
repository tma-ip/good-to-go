﻿using GoodToGo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodToGo.Repository
{
    public class FuelConsumptionRepository : GenericRepository<FuelConsumption>, IFuelConsumptionRepository
    {
        public FuelConsumptionRepository (ApplicationDbContext context) : base(context)
        { }

        public IEnumerable<FuelConsumption> GetAllByCarId(int carId)
        {
            return context.FuelConsumptions.Where(x => x.CarId == carId).ToList();
        }
    }
}