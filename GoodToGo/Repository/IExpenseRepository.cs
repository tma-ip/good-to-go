﻿using GoodToGo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodToGo.Repository
{
    public interface IExpenseRepository : IGenericRepository<Expense>
    {
        IEnumerable<Expense> GetAllByCarId(int carId);
        IEnumerable<Expense> GetAll();
    }
}
