﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodToGo.Repository
{
    public interface IUnitOfWork
    {
        IUserRepository Users { get;  }
        ICarRepository Cars { get;  }
        IExpenseRepository Expenses { get;  }
        IFuelConsumptionRepository FuelConsumptions { get;  }

        int Save();
    }
}
