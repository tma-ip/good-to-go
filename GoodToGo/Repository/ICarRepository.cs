﻿using GoodToGo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodToGo.Repository
{
    public interface ICarRepository:IGenericRepository<Car>
    {
        IEnumerable<Car> SelectAllByUser(string userId);
        Car SelectByID(int id);
        string GetName(int carId);
    }
}
