﻿using GoodToGo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodToGo
{
    public class SingletonTrainPredict
    {

        private static volatile SingletonTrainPredict instance;
        //in cazul mai multor threaduri
        private static object syncRoot = new Object();

        private float m1, b1;


        private SingletonTrainPredict() { }

        public float GetM()
        {
            return m1;
        }

        public float GetB()
        {
            return m1;
        }

        public void LROrdinaryLeastSquares(List<FuelConsumption> data)
        {
            m1 = 0;
            b1 = 0;
            float xSum = 0;
            float x2Sum = 0;
            float ySum = 0;
            float y2Sum = 0;
            float xySum = 0;
            int nr = data.Count();
            foreach (var data1 in data)
            {
                xSum += (float)data1.Distance;
                x2Sum += (float)data1.Distance * (float)data1.Distance;
                ySum += (float)data1.Fuel;
                y2Sum += (float)data1.Fuel * (float)data1.Fuel;
                xySum += (float)data1.Fuel * (float)data1.Distance;
            }

            float imp = (nr * x2Sum - xSum * xSum);//impartitor
            if (imp == 0)
            {
                // nu se poate rezolva problema 
                m1 = 0;
                b1 = 0;
            }
            else
            {
                m1 = (nr * xySum - xSum * ySum) / imp;
                b1 = (ySum * x2Sum - xSum * xySum) / imp;
            }
        }

        public float PredictOrdinaryLeastSquares(float distance)
        {
            float fuel = 0;
            fuel = m1 * distance + b1;
            return fuel;
        }

        public static SingletonTrainPredict Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new SingletonTrainPredict();
                    }
                }

                return instance;
            }
        }
    }
}