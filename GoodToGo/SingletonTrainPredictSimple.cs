﻿using GoodToGo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodToGo
{
    public class SingletonTrainPredictSimple
    {
        private static volatile SingletonTrainPredictSimple instance;
        //in cazul mai multor threaduri
        private static object syncRoot = new Object();

        private float m1, b1;


        private SingletonTrainPredictSimple() { }

        public float GetM()
        {
            return m1;
        }

        public float GetB()
        {
            return m1;
        }

        public void LROrdinaryLeastSquares(List<FuelConsumption> data)
        {
            m1 = 0;
            b1 = 0;
            float xSum = 0;
            float ySum = 0;
            int nr = data.Count();
            foreach (var dataC in data)
            {
                xSum += (float)dataC.Distance;
                ySum += (float)dataC.Fuel;
            }
            float xMed = xSum / nr;
            float yMed = ySum / nr;

            float deimp = 0;
            float imp = 0;
            foreach (var dataC in data)
            {
                float xC = (float)dataC.Distance;
                float yC = (float)dataC.Fuel;
                deimp += (xC - xMed) * (yC - yMed);
                imp += (xC - xMed) * (xC - xMed);
            }

            if (imp == 0)
            {
                // nu se poate rezolva problema 
                m1 = 0;
                b1 = 0;
            }
            else
            {
                m1 = deimp / imp;
                b1 = yMed - m1*xMed;
            }
        }

        public float PredictOrdinaryLeastSquares(float distance)
        {
            float fuel = 0;
            fuel = m1 * distance + b1;
            return fuel;
        }

        public static SingletonTrainPredictSimple Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new SingletonTrainPredictSimple();
                    }
                }

                return instance;
            }
        }
    }
}